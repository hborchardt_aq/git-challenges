Convert merge commits into normal commits
-----

Say you have the following history:

```
_B___a___b_____c_____d___M___g__ - c1/develop
  \_________e_____f_____/        - c1/origin-develop
   \_________________________h__ - c1/master
```
This can happen, for example, if someone else changed commits on the origin/develop and you do `git pull` with the merge strategy (instead of the rebase strategy).

You merged the two branches, which created a merge commit `M`, painfully resolving conflicts, and `g` already contains important changes.

The objective is to rebase develop onto master.

If we try to do this right away (`git rebase c1/master`), git tries to apply the patches in order, but this fails:

```
_B___a___b_____c_____d___e_f___g__
                         ^ it fails while applying e, because e applies cleanly to B,
                           but a to d already changed the code too much. Resolving this
                           already was done while creating the merge commit M, so let's
                           use that information
$ git checkout c1/develop
$ git rebase c1/master
error: could not apply db7d423... e
...
Auto-merging file.txt
CONFLICT (content): Merge conflict in file.txt
```

The plan is to remove the merge commit and squash all the commits of the origin/develop branch, including the merge commit, into one normal commit.
In other words, this will produce a commit history like this:
```
_B___a___b_____c_____d___m'___g'__ - develop
```
where `m'` is just a normal commit, and there is no difference between `g` and `g'` (so `git diff g g'` is empty).

The approach is to do an interactive rebase. We rewrite the history to have commits that look exactly like the commit history that we want. 
Because the commit `m'` does not exist yet in any meaningful way, we
 - insert a dummy commit at its place (e.g. any commit from the origin-develop branch)
 - set it to `edit` during rebasing
 - while editing, check out exactly the state after the merge commit



Do the following:
```
# get an overview of the situation
$ git log --graph --decorate --pretty=oneline --abbrev-commit
* 1ede6ca (HEAD -> develop, origin/c1/develop) g
*   0919f8b Merge branch 'origin-develop' into develop
|\
| * 5818e6d (origin/c1/origin-develop, origin-develop) f
| * db7d423 e
* | 666d84b d
* | 8597039 c
* | a10e74d b
* | d81e35d a
|/
* 1a0b34d B
# rebase interactively on the branch point B and delete all commits from the other branch,
# so remove all commits (e and f) of origin-develop
# but keep one as a dummy to edit
$ git rebase -i 1a0b34d
pick d81e35d a
pick a10e74d b
pick 8597039 c
pick 666d84b d
edit db7d423 e
pick 1ede6ca g

# ... now, we are editing the one commit from the other branch
$ git rev-parse HEAD
666d84b0f0446569ebaddb1513b53a2eaa5c458b
# note down the current git hash of this state (e.g. 666d84b0f0446569ebaddb1513b53a2eaa5c458b)

# now change the tree to look exactly like it is after M
# do a `git reset --hard M`
$ git reset --hard 0919f8b25a9f949a8a3e30c2f40871941fa83fe4

# now checkout the noted rebase edit state 666d84b0f0446569ebaddb1513b53a2eaa5c458b, but don't change any of the files
# add --soft to stage all the changes already
$ git reset --soft 666d84b0f0446569ebaddb1513b53a2eaa5c458b
Unstaged changes after reset:
M       file.txt

# now the changes are staged, and we just need to change the commit message to something appropriate
git rebase --continue
```
